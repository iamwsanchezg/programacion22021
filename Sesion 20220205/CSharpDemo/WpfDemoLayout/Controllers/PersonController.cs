﻿using Entities.Models;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using WpfDemoLayout.Views;

namespace WpfDemoLayout.Controllers
{
    public class PersonController
    {
        private PersonWindow pw;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public PersonController(PersonWindow personWindow)
        {
            pw = personWindow;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public void PersonButtonEventHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "SaveButton":
                    SaveDataToFile();
                    break;
                case "SaveToBinaryButton":
                    SaveDataToBinaryFile();
                    break;
                case "SaveToXmlButton":
                    SaveDataToXmlFile();
                    break;
                case "SelectButton":
                    OpenDataFile();
                    break;
            }
        }

        private void SaveDataToXmlFile()
        {
            sfdialog.Filter = "Xml File (*.xml)|*.xml";
            if (sfdialog.ShowDialog() == true)
            {
                Person p = pw.GetData();
                p.ToXml(sfdialog.FileName);
            }
        }

        private void SaveDataToBinaryFile()
        {
            sfdialog.Filter = "Binary File (*.bin)|*.bin";
            if (sfdialog.ShowDialog() == true)
            {
                Person p = pw.GetData();
                p.ToBinary(sfdialog.FileName);
            }
        }

        private void OpenDataFile()
        {
            ofdialog.Filter = "Json File (*.json)|*.json";
            if (ofdialog.ShowDialog() == true)
            {
                Person p = new Person();
                pw.SetData(p.FromJson(ofdialog.FileName));
            }
        }

        private void SaveDataToFile()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Person p = pw.GetData();
                p.ToJson(sfdialog.FileName);
            }
        }
    }
}
