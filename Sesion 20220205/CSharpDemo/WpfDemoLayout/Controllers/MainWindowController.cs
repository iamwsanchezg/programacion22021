﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WpfDemoLayout.Views;

namespace WpfDemoLayout.Controllers
{
    public class MainWindowController
    {
        MainWindow mainWindow;
        public MainWindowController(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }
        public void MainWindowMenuItemEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem menu = (MenuItem)sender;
            switch (menu.Name)
            {
                case "GridLayoutMenuItem":
                    ShowGridLayoutDemo();
                    break;
                case "WrapPanelLayoutMenuItem":
                    ShowWrapPanelLayoutDemo();
                    break;
                case "DockLayoutMenuItem":
                    ShowDockLayoutDemo();
                    break;
                case "ExitMenuItem":
                    Environment.Exit(0);
                    break;
                case "PersonFormMenuItem":
                    MostraFormularioPersona();
                    break;

            }
        }

        private void MostraFormularioPersona()
        {
            PersonWindow personForm = new PersonWindow();
            personForm.Show();
        }

        private void ShowDockLayoutDemo()
        {
            DockLayoutDemo dld = new DockLayoutDemo();
            dld.Show();
        }

        private void ShowWrapPanelLayoutDemo()
        {
            WrapPanelLayout wpl = new WrapPanelLayout();
            wpl.Show();
        }

        private void ShowGridLayoutDemo()
        {
            GridLayoutDemo gld = new GridLayoutDemo();
            gld.Show();
        }
    }
}
