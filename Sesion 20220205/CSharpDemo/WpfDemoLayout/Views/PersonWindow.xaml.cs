﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfDemoLayout.Controllers;

namespace WpfDemoLayout.Views
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class PersonWindow : Window
    {
        PersonController pc;
        public PersonWindow()
        {
            InitializeComponent();
            SetupController();
        }
        public void SetupController()
        {
            pc = new PersonController(this);
            RoutedEventHandler routed = new RoutedEventHandler(pc.PersonButtonEventHandler);
            this.SaveButton.Click += routed;
            this.SaveToBinaryButton.Click += routed;
            this.SaveToXmlButton.Click += routed;
            this.SelectButton.Click += routed;
        }
        public Person GetData()
        {
            Person person = new Person
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                BirthDate = (DateTime)BirthDatePicker.SelectedDate,
                BirthPlace = BirthPlaceComboBox.SelectedValue.ToString()
            };
            return person;
        }

        public void SetData(Person person)
        {
            FirstNameTextBox.Text = person.FirstName;
            LastNameTextBox.Text = person.LastName;
            BirthDatePicker.SelectedDate = person.BirthDate;
            BirthPlaceComboBox.SelectedValue = person.BirthPlace;
        }
    }
}
