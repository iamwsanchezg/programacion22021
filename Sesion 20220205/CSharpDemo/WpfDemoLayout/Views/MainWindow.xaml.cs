﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfDemoLayout.Controllers;

namespace WpfDemoLayout.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowController maincontroller;
        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }
        private void SetupController()
        {
            maincontroller = new MainWindowController(this);
            RoutedEventHandler routed = new RoutedEventHandler(maincontroller.MainWindowMenuItemEventHandler);
            this.WrapPanelLayoutMenuItem.Click += routed;
            this.GridLayoutMenuItem.Click += routed;
            this.DockLayoutMenuItem.Click += routed;
            this.ExitMenuItem.Click += routed;
            this.PersonFormMenuItem.Click += routed;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }


    }
}
