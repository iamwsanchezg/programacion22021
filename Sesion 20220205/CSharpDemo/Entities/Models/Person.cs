﻿using DemoWPF3.Tools.Misc;
using DemoWPF3.Tools.Serialization;
using Entities.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Serializable]
    public class Person:IToFile,IFromFile<Person>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get { return Util.GetAge(BirthDate); } }
        public string BirthPlace { get; set; }
        public Person FromBinary(string filepath)
        {
            return BinarySerialization.ReadFromBinaryFile<Person>(filepath);
        }
        public Person FromJson(string filepath)
        {
            return JsonSerialization.ReadFromJsonFile<Person>(filepath);
        }
        public Person FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Person>(filepath);
        }
        public int GetAge()
        {
            return DateTime.Now.Year - BirthDate.Year;
        }
        public void show()
        {
            Console.WriteLine(string.Format("Primer nombre: {0}", FirstName));
            Console.WriteLine(string.Format("Apellido: {0}", LastName));
            Console.WriteLine(string.Format("Fecha de nacimiento: {0}", BirthDate.ToString("dddd, dd MMMM yyyy")));

            Console.WriteLine(string.Format("Edad dese atributo: {0}, edad desde función: {1}", Age, GetAge()));
        }
        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }
        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }
        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
