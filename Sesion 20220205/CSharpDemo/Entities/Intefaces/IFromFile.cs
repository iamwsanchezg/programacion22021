﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Intefaces
{
    public interface IFromFile<T>
    {
        T FromXml(string filepath);
        T FromJson(string filepath);
        T FromBinary(string filepath);
    }
}
