﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Intefaces
{
    public interface IToFile
    {
        void ToXml(string filepath);
        void ToJson(string filepath);
        void ToBinary(string filepath);

    }
}
