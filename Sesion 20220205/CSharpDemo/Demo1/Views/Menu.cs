﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1.Views
{
    public class Menu
    {
        public int SelectedOption { get; set; }
        public decimal O1 { get; set; }
        public decimal O2 { get; set; }
        public void Show()
        {
            Console.WriteLine("Elija una opción");
            Console.WriteLine("1. Sumar");
            Console.WriteLine("2. Restar");
            Console.WriteLine("3. Salir");
        }
        public bool Read()
        {
           bool output = int.TryParse(Console.ReadLine(), out int x);
            SelectedOption = x;
            return output;
        }
        public bool IsValid()
        {
            bool isValid = Read();
            return (isValid && SelectedOption >= 1 && SelectedOption <= 3);
        }

        public void Evaluate()
        {
            
            if (IsValid())
            {
              Execute(SelectedOption);
              Show();
              Evaluate();
            }
            else
            {
                Console.WriteLine("Entrada no válida");
            }
        }

        private void Execute(int option)
        {            
            switch (option)
            {
                case 1:
                    GetOperator();
                    ExecuteSum();
                    break;
                case 2:
                    GetOperator();
                    ExecuteTakeaway();
                    break;
                case 3:
                    Environment.Exit(0);
                    break;
            }
        }
        private void GetOperator()
        {
            Console.WriteLine("Digita el operador 1:");
            decimal.TryParse(Console.ReadLine(), out decimal o1);
            O1 = o1;
            Console.WriteLine("Digita el operador 2:");
            decimal.TryParse(Console.ReadLine(), out decimal o2);
            O2 = o2;
        }
        private void ExecuteTakeaway()
        {
            Console.WriteLine(string.Format("El resultado es: {0}", O1 - O2));
        }

        private void ExecuteSum()
        {
            Console.WriteLine(string.Format("El resultado es: {0}", O1 + O2));
        }
    }
}
