﻿using Demo1.Misc;
using Demo1.Views;
using Entities.Models;
using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //Declarations declarations = new Declarations();
            //decimal value = declarations.divide();
            //Console.WriteLine("El valor es: " + value.ToString());
            //Console.WriteLine("Indica a: ");
            //int.TryParse(Console.ReadLine(), out int i);
            //Console.WriteLine("Indica b: ");
            //int.TryParse(Console.ReadLine(), out int j);
            //Console.WriteLine("La suma es: " + (i + j).ToString());

            //Console.WriteLine("Indica una fecha: ");
            //DateTime userDateTime;
            //if (DateTime.TryParse(Console.ReadLine(), out userDateTime))
            //{
            //    Console.WriteLine("El día de la semana es: " + userDateTime.DayOfWeek);
            //}
            //else
            //{
            //    Console.WriteLine("Has proporcionado un valor incorrecto.");
            //}

            //Animal a = new Animal();
            //Cat b = new Cat();
            //Dog c = new Dog();
            //a.AnimalSound();
            //b.AnimalSound();
            //c.AnimalSound();
            //Teacher person = new Teacher();
            //person.FirstName = "William";
            //person.LastName = "Sanchez";
            //person.BirthDate = new DateTime(1983, 9, 1);
            //person.Create();

            //Student person1 = new()
            //{
            //    FirstName = "Rebeca",
            //    LastName = "Almeida",
            //    BirthDate = new DateTime(1985, 9, 1)
            //};
            //person1.Create();

            //var person2 = new Person
            //{
            //    FirstName = "Juan",
            //    LastName = "López",
            //    BirthDate = new DateTime(1990, 1, 1)
            //};
            //person2.show();

            Menu menu = new Menu();
            do
            {
                menu.Show();
                menu.Evaluate();
            } while (menu.IsValid());

        }
    }
}
