/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc2.models.entities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author willj
 */
public class Person implements Serializable{
    protected String firstName;
    protected String lastName;

    public Person() {
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public void save(File f){
        try{
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
            os.writeObject(this); 
            os.flush();
            os.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public Person Open(File f){
        try{
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(f));
            Person p = (Person)is.readObject();
            is.close();
            return p;
        }
        catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }    
}
