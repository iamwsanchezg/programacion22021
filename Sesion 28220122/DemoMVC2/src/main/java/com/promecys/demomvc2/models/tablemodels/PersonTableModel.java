/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc2.models.tablemodels;

import com.promecys.demomvc2.models.entities.Person;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author willj
 */
public class PersonTableModel extends AbstractTableModel{
    private String columnNames[];
    private Object rowData[][];
    Class[] types;
    boolean[] canEdit;
    public PersonTableModel(){
        setup();
    }
    public void bindTableData(Person[] personList){
       rowData = new Object[personList.length][columnNames.length];
       int c=0;
       for(Person person:personList){
           rowData[c] = new Object[]{
               person.getFirstName(),
               person.getLastName()
           };
           
           c++;
       }
    }
    public TableModel getModel(){
       TableModel model = new DefaultTableModel(rowData,columnNames)
        {
            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
       return model; 
    }
    
    public void setDataModel(Object[][] data){
        rowData = data;
    }
    public Person[] getData(){
        Person [] personList = null;
        if(rowData!=null){
            personList = new Person[rowData.length];
            for(int i=0;i<rowData.length;i++){
                Person p = new Person();
                p.setFirstName((String)rowData[i][0]);
                p.setLastName((String)rowData[i][1]);                
                personList[i] = p;
            }
        }
        else
        {
            personList = new Person[0];
        }
        return personList;
    }
        
    private void setup(){
        Field fields[] = Person.class.getDeclaredFields();
        columnNames = new String[fields.length];
        types = new Class[fields.length];
        canEdit = new boolean[fields.length];
        int c=0;
        for(Field field:fields){
            columnNames[c] =field.getName();
            types[c] = field.getType();
            canEdit[c] = true;
            c++;
        }
    }

    @Override
    public int getRowCount() {
        return rowData!=null?rowData.length:0;
    }

    @Override
    public int getColumnCount() {
        return columnNames!=null?columnNames.length:0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }
    
    public void loadList(File f){
        try{
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(f));
            Person[] pl = (Person[])is.readObject();
            is.close();
            bindTableData(pl);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void saveList(File f, Person[] pl){
        try{
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
            os.writeObject(pl); 
            os.flush();
            os.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
