/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc2.controllers;

import com.promecys.demomvc2.views.NotepadForm;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author willj
 */
public class NotepadController implements ActionListener{
    private NotepadForm notepad;

    public NotepadController() {
        notepad = new NotepadForm();
    }

    public NotepadController(NotepadForm notepad) {
        this.notepad = notepad;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "openFile":
                showFile();
                break;
            case "saveFile":
                saveFile();
                break;
                
        }
    }
    private void showFile(){
        notepad.showText(openFile());
    }
    private String openFile() {
       JFileChooser fd = new JFileChooser();
       fd.showOpenDialog(notepad);
       File f = fd.getSelectedFile();
        BufferedReader br = null;
        try {
            StringBuilder sb = new StringBuilder();           
            br = new BufferedReader(new FileReader(f));
            String strLine = br.readLine();
            while (strLine != null)
            {
                sb.append(strLine);
                sb.append(System.lineSeparator());
                strLine = br.readLine();
            }
            br.close();
            return sb.toString();
        } catch (Exception ex) {
            Logger.getLogger(NotepadController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return "";
        }       
    }

    private void saveFile() {
        try {
            JFileChooser fc = new JFileChooser();
            fc.showSaveDialog(notepad);
            if(fc.getSelectedFile()!=null){
                FileWriter myWriter = new FileWriter(fc.getSelectedFile());
                myWriter.write(notepad.getText());
                myWriter.close();
            }

        } catch (IOException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
    }
    
}
