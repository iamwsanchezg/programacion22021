/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc2.controllers;

import com.promecys.demomvc2.models.entities.Person;
import com.promecys.demomvc2.models.tablemodels.PersonTableModel;
import com.promecys.demomvc2.views.PersonForm;
import com.promecys.demomvc2.views.PersonListForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author willj
 */
public class PersonController implements ActionListener {
    Person person;
    PersonForm personForm;
    JFileChooser dlg;
    PersonListForm personListForm;
    PersonTableModel personTableModel;
    
    public PersonController(PersonForm personForm) {
        this.personForm = personForm;
        this.dlg = new JFileChooser();
    }
    public PersonController(PersonListForm personListForm) {
        this.personListForm = personListForm;
        this.dlg = new JFileChooser();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "save":
                save();
                break;
            case "cancel":
                cancel();
                break;
            case "select":
                open();
                break;
            case "openList":
                openList();
                break;
            case "saveList":
                saveList();
                break; 
            case "closeForm":
                personListForm.dispose();
                break;
            case "newRow":
                personListForm.newRow();
                break;
            case "removeRow":
                personListForm.removeRow();
                break;
        }
    }

    private void save() {
       dlg.showSaveDialog(personForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
           person = personForm.getData();
           person.save(f);
       }
    }

    private void cancel() {
      personForm.dispose();
    }

    private void open() {
       dlg.showOpenDialog(personForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
           person = new Person();
           personForm.showData(person.Open(f));
           
       }
    }
    
    private void openList(){
       dlg.showOpenDialog(personListForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
            personTableModel = new PersonTableModel();
            personTableModel.loadList(f);
            personListForm.showData(personTableModel.getModel());         
       } 
    }

    private void saveList() {
       dlg.showSaveDialog(personForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
           Person[] personList = personListForm.getData();
           personTableModel = new PersonTableModel();
           personTableModel.saveList(f, personList);
       }
    }
}
