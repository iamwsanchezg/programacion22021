/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wssoftware.demonb3.controllers;

import com.wssoftware.demonb3.models.CustomFile;
import com.wssoftware.demonb3.views.EditorFrame;
import com.wssoftware.demonb3.views.MainFrame;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author willj
 */
public class EditorController implements ActionListener{
    private JFileChooser fc; 
    private MainFrame parent;
    private EditorFrame editor;
    public EditorController(EditorFrame editor){
        this.editor = editor;
        fc = new JFileChooser();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand())
        {
            case "txtFile":
                showNewForm(EditorFrame.TXT_FILE);
                break;
             case "xmlFile":
                showNewForm(EditorFrame.XML_FILE);
                break;      
            case "jsonFile":
                showNewForm(EditorFrame.JSON_FILE);
                break;                
            case "open":
                showOpenFileDialog();
                break;
            case "save":
                showSaveFileDialog();
                break;
            case "cut":
                editor.cutText();
                break;
            case "copy":
                editor.copyText();
                break;
            case "paste":
                 editor.pasteText();               
                break; 
            case "close":
                this.editor.dispose();
                break;
        }
    }
    public void showNewForm(int type){
        EditorFrame neweditor = new EditorFrame(type);
        if(parent==null){
            JDesktopPane desktop = (JDesktopPane)editor.getParent();
            parent = (MainFrame)desktop.getRootPane().getParent();
        }
        parent.showChild(neweditor, true);
    }
    private void showFileInForm(CustomFile customfile)
    {
        editor.show(customfile);
    }
    private File showOpenFileDialog(){
        File file = null;
        if (fc.showOpenDialog(editor)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                readFile(file);
            }
            else{
               JOptionPane.showMessageDialog(editor, "No se seleccionó archivo");
            }
        }
        return file;
    }
    
    private void readFile(File file) {
        BufferedReader br = null;
        try {
            StringBuilder sb = new StringBuilder();           
            br = new BufferedReader(new FileReader(file));
            String strLine = br.readLine();
            while (strLine != null)
            {
                sb.append(strLine);
                sb.append(System.lineSeparator());
                strLine = br.readLine();
            }
            br.close();
            CustomFile myCustomFile = new CustomFile();
            myCustomFile.setFileName(file.getName());
            myCustomFile.setContent(sb.toString());
            showFileInForm(myCustomFile);            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void showSaveFileDialog(){
       File file;
       if (fc.showSaveDialog(editor)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){                  
                writeFile(file, getDataFromForm());
            }
            else{
               JOptionPane.showMessageDialog(editor, "No se seleccionó archivo", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
    
    private CustomFile getDataFromForm(){
        CustomFile cf = null;        
        cf = editor.getData();
        return cf;
    }
    
    private void writeFile(File file, CustomFile myFile){      
        try {
            Writer w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            w.write(myFile.getContent());
            w.flush();
            w.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void copyToClipboard(){
        String myString = editor.getSelectedText();
        StringSelection stringSelection = new StringSelection(myString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }
}
