/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wssoftware.demonb3.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author willj
 */
public class FileTableModel extends AbstractTableModel {

    private String[][] data;
    private final Object columnNames[] = {"Nombre de archivo", "Ruta", "Fecha de creación"};

    public FileTableModel() {
        data = new String[][]{
            {null, null}
        };
    }

    public FileTableModel(String f[][]) {
        data = f;
    }

    public FileTableModel(File f) {
        File files[] = f.listFiles();
        String[][] xdata = new String[files.length][3];
        int c = 0;
        for (File file : files) {
            try {
                xdata[c][0] = file.getName();
                xdata[c][1] = file.getAbsolutePath();
                BasicFileAttributes attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                xdata[c][0] = file.getName();
            } catch (IOException ex) {
                Logger.getLogger(FileTableModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        setDataModel(xdata);
    }

    public void setDataModel(String f[][]) {
        data = f;
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
