﻿using DemoWPF3.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWPF3.HRAdmin.ViewModels
{
    public class GroupViewModel
    {
        public string Title { get; set; }
        public Group Group { get; set; }
    }
}
