﻿using DemoWPF3.Entities.Models;
using DemoWPF3.HRAdmin.ViewModels;
using DemoWPF3.HRAdmin.Views.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DemoWPF3.HRAdmin.Controllers
{
    public class MainWindowController
    {
        private MainWindow mainWindow;
        public MainWindowController(MainWindow window)
        {
            mainWindow = window;
        }
        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;
                case "PersonMenuItem":
                    mainWindow.DataContext = new PersonViewModel();
                    break;
                case "PersonListMenuItem":
                    mainWindow.DataContext = new GroupViewModel() { Title = "Título de la ventana", Group = GetGroup() };
                    break;


            }
        }
        private Group GetGroup()
        {
            Group g = new Group
            {
                Name = "Los 4 Fantásticos",
                Members = new List<Person>
                    {
                        new Person() { FirstName = "Peter", LastName = "Parker", BirthDate = new DateTime(2000, 1, 1), State = "New York" },
                        new Person() { FirstName = "Susan", LastName = "Storm", BirthDate = new DateTime(1985, 1, 1), State = "New York" },
                        new Person() { FirstName = "Johny", LastName = "Storm", BirthDate = new DateTime(1985, 1, 1),State="New York" },
                        new Person() { FirstName = "Reed", LastName = "Richards", BirthDate = new DateTime(1980, 1, 1), State="New York" }
                    }
            };
            return g;
        }
    }
}
