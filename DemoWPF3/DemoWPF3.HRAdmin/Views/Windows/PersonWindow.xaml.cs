﻿using DemoWPF3.Entities.Models;
using DemoWPF3.HRAdmin.Controllers;
using System;
using System.Windows;
using DemoWPF3.Entities.Interfaces;

namespace DemoWPF3.HRAdmin.Views.Windows
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class PersonWindow : Window, IForm<Person>
    {
        PersonController pc;
        public PersonWindow()
        {
            InitializeComponent();
            //centrar la ventana
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //Inicializar controlador
            SetupController();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Person GetData()
        {
            Person person = new Person();
            person.FirstName = FirstNameTextBox.Text;
            person.LastName = LastNameTextBox.Text;
            person.BirthDate = (DateTime)BirthDatePicker.SelectedDate;
            //person.State = ((ComboBoxItem)StateCombobox.SelectedValue).Content.ToString();
            person.State = StateCombobox.SelectedValue.ToString();
            return person;
        }

        public void SetData(Person person)
        {
            FirstNameTextBox.Text = person.FirstName;
            LastNameTextBox.Text = person.LastName;
            BirthDatePicker.SelectedDate = person.BirthDate;
            StateCombobox.SelectedValue = person.State;
        }

        protected void SetupController()
        {
            pc = new PersonController(this);
            this.SaveButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }
    }
}
