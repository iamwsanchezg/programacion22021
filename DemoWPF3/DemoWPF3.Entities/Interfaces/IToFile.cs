﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Interfaces
{
    public interface IToFile
    {
        void ToXml(string filepath);
        void ToJson(string filepath);
        void ToBinary(string filepath);

    }
}
