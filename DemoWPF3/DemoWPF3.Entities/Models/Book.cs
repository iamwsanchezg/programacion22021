﻿using DemoWPF3.Entities.Interfaces;
using DemoWPF3.Tools.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Models
{
    [Serializable]
    public class Book:IToFile,IFromFile<Book>
    {
        public string ISBN { get; set; } 
        public string Title { get; set; }
        public string Author { get; set; }

        public Book FromBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public Book FromJson(string filepath)
        {
            throw new NotImplementedException();
        }

        public Book FromXml(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
