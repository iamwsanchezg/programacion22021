﻿using Lib.Misc;
using Lib.Security;
using Mailer.Models;
using Mailer.Properties;
using Mailer.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Mailer.Controllers
{
    public class MailEditorController
    {
        private MailEditor mailEditor;
        public MailEditorController(MailEditor mailEditor)
        {
            this.mailEditor = mailEditor;
        }
        public void MailEditorEventHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "CleanButton":
                    mailEditor.CleanEditor();
                    break;
                case "SendButton":
                    SendMail();
                    break;
            }
        }
        public void SendMail()
        {
            bool sent = false;
            bool AllAreValidAccounts = true;
            string ouputmessage = "";                                

            string SMTPMailServer = Settings.Default.SMTPMailServer;
            int IncomingMailPort = Settings.Default.IncomingMailPort;
            int OutgoingMailPort = Settings.Default.OutgoingMailPort;
            bool SSLMode = Settings.Default.SSLMode;
            string MailAddress = Settings.Default.MailAddress;
            string DisplayName = Settings.Default.DisplayName;

            string phrase = Environment.MachineName + Environment.UserName;
            string Password = StringCipher.Decrypt(Settings.Default.Password, phrase);
            EmailDocument document = mailEditor.GetData();
            MailMessage msg = new MailMessage();
            SmtpClient client = new SmtpClient();

            string[] emailAddresses = document.To.Split(',');
            
            foreach (var emailAddress in emailAddresses)
            {
                if (!emailAddress.IsValidEmail())
                {
                    AllAreValidAccounts = false;
                    ouputmessage += "Una o mas direcciones de correo electrónico no son válidas";
                    break;
                }
            }
            if (AllAreValidAccounts && document.Bcc.Length > 0)
            {
                emailAddresses = document.Bcc.Split(',');
                foreach (var emailAddress in emailAddresses)
                {
                    if (!Network.IsValidEmail(emailAddress))
                    {
                        AllAreValidAccounts = false;
                        ouputmessage += "Una o mas direcciones de correo electrónico Cco no son válidas";
                        break;
                    }
                }
            }
            if (AllAreValidAccounts && document.Cc.Length > 0)
            {
                emailAddresses = document.Cc.Split(',');
                foreach (var emailAddress in emailAddresses)
                {
                    if (!Network.IsValidEmail(emailAddress))
                    {
                        AllAreValidAccounts = false;
                        ouputmessage += "Una o mas direcciones de correo electrónico Cc no son válidas";
                        break;
                    }
                }
            }
            if (AllAreValidAccounts)
            {
                try
                { 
                    //Inicializar parámetros de conexión
                    client.Credentials = new NetworkCredential(MailAddress, Password);
                    client.Host = SMTPMailServer;
                    client.Port = OutgoingMailPort;
                    client.EnableSsl = SSLMode;

                    //Inicializar parámetros de mensaje
                    msg.To.Add(document.To);
                    if (document.Bcc.Length > 0) msg.Bcc.Add(document.Bcc);
                    if (document.Cc.Length > 0) msg.CC.Add(document.Cc);
                    msg.From = new MailAddress(MailAddress, DisplayName, Encoding.Unicode);
                    //if (!string.IsNullOrEmpty(AttachedFile))
                    //{
                    //    msg.Attachments.Add(new Attachment(AttachedFile));
                    //}
                    msg.Subject = document.Subject;
                    msg.SubjectEncoding = Encoding.UTF8;
                    msg.Body = document.Message;
                    msg.BodyEncoding = Encoding.UTF8;
                    msg.IsBodyHtml = false;

                    client.Send(msg);
                    sent = true;
                    MessageBox.Show("Correo enviado exitosamente.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (SmtpException ex)
                {
                    sent = false;
                    ouputmessage = ex.Message;
                    MessageBox.Show(ouputmessage, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Console.WriteLine(ouputmessage);
                }
                catch (Exception ex)
                {
                    sent = false;
                    ouputmessage = ex.Message;
                    MessageBox.Show(ouputmessage, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Console.WriteLine(ouputmessage);
                }
            }
            msg.Dispose();
        }
    }
}
