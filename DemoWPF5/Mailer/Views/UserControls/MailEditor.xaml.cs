﻿using Lib.Misc;
using Mailer.Controllers;
using Mailer.Models;
using System.Windows;
using System.Windows.Controls;

namespace Mailer.Views.UserControls
{
    /// <summary>
    /// Interaction logic for MailEditor.xaml
    /// </summary>
    public partial class MailEditor : UserControl
    {
        MailEditorController mec;
        public MailEditor()
        {
            InitializeComponent();
            SetupController();
        }
        private void SetupController()
        {
            mec = new MailEditorController(this);
            RoutedEventHandler routed = new RoutedEventHandler(mec.MailEditorEventHandler);
            CleanButton.Click += routed;
            SendButton.Click += routed;
        }
        public void CleanEditor()
        {
            ToTextBox.Text = "";
            CcTextBox.Text = "";
            BccTextBox.Text = "";
            SubjectTextBox.Text = "";
            MessageRichTextBox.Document.Blocks.Clear();
        }
        public EmailDocument GetData()
        {
            EmailDocument ed = new EmailDocument()
            {
                To = ToTextBox.Text,
                Cc = CcTextBox.Text,
                Bcc = BccTextBox.Text,
                Subject = SubjectTextBox.Text,
                Message = MessageRichTextBox.GetPlainText()
            };
            return ed;
        }
    }
}
