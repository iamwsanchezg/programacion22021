/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc1.controllers;

import com.promecys.demomvc1.views.MainForm;
import com.promecys.demomvc1.views.PersonForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author willj
 */
public class MainController implements ActionListener{
    MainForm mainForm;

    public MainController(MainForm mainForm) {
        this.mainForm = mainForm;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "person":
                showPerson();
                break;
            case "exit":
                System.exit(0);
                break;
        }
    }

    private void showPerson() {
        PersonForm pf = new PersonForm();
        mainForm.showChild(pf);
    }
    
}
