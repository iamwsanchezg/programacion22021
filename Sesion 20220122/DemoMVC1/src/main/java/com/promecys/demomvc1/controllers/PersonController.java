/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.promecys.demomvc1.controllers;

import com.promecys.demomvc1.models.Person;
import com.promecys.demomvc1.views.PersonForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author willj
 */
public class PersonController implements ActionListener {
    Person person;
    PersonForm personForm;
    JFileChooser dlg;

    public PersonController(PersonForm personForm) {
        this.personForm = personForm;
        this.dlg = new JFileChooser();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "save":
                save();
                break;
            case "cancel":
                cancel();
                break;
            case "select":
                open();
                break;
        }
    }

    private void save() {
       dlg.showSaveDialog(personForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
           person = personForm.getData();
           person.save(f);
       }
    }

    private void cancel() {
      personForm.dispose();
    }

    private void open() {
        dlg.showOpenDialog(personForm);
       File f = dlg.getSelectedFile();
       if(f!=null){
           person = new Person();
           personForm.showData(person.Open(f));
           
       }
    }
}
