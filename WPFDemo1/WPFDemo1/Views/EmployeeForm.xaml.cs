﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFDemo1.Models;

namespace WPFDemo1.Views
{
    /// <summary>
    /// Interaction logic for EmployeeForm.xaml
    /// </summary>
    public partial class EmployeeForm : Window
    {
        public EmployeeForm()
        {
            InitializeComponent();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Employee employee = new Employee()
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                BirthDate = (DateTime)BirthDatePicker.SelectedDate,
                Wage = Convert.ToDouble(WageTextBox.Text),
                WorkedDays = 25
            };

            MessageBox.Show(string.Format("La edad del empleado es {0} y su pago neto es {1}",
                employee.Age, employee.getSalary()));

        }
    }
}
