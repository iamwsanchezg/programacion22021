﻿using HRManagment.ViewModels;
using HRManagment.Views.UserControls;
using System.Windows;
using System.Windows.Forms;
using Button = System.Windows.Controls.Button;

namespace HRManagment.Controllers
{
    public class SettingsController
    {
        SettingsDialog Dialog;
        public SettingsController(SettingsDialog dialog)
        {
            Dialog = dialog;
        }
        public void SettingsEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "OkButton":
                    SaveSettings();
                    break;
                case "ChooseButton":
                    ShowFolderBrowser();
                    break;
                case "CancelButton":
                    Dialog.Hide();
                    break;
            }
        }

        private void SaveSettings()
        {
            ((SettingsViewModel)Dialog.DataContext).SaveSettings();
        }

        private void ShowFolderBrowser()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            DialogResult result = folderBrowser.ShowDialog();
            if (result.ToString() != string.Empty)
            {
                Dialog.SetDirectory(folderBrowser.SelectedPath);
            }
        }
    }
}
