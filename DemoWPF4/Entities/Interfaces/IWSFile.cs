﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Interfaces
{
    public interface IWSFile<T>
    {
        void ToJson(string Path);
        T FromJson(string Path);
    }
}
