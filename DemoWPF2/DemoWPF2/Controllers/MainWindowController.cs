﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DemoWPF2.Controllers
{
    public class MainWindowController
    {
        MainWindow mainWindow;
        public MainWindowController(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }
        public void MainWindowMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem menu = (MenuItem)sender;
            switch (menu.Name)
            {
                case "GridLayoutDemoMenuItem":
                    ShowGridLauoutDemo();
                    break;
                case "WrapLayoutMenuItem":
                    ShowWrapLayoutDemo();
                    break;
            }
        }

        private void ShowWrapLayoutDemo()
        {
            WrapPanelDemo wpd = new WrapPanelDemo();
            wpd.Show();
        }

        private void ShowGridLauoutDemo()
        {
            GridLayoutDemo gld = new GridLayoutDemo();
            gld.Show();
        }
    }
}
