﻿using DemoWPF2.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DemoWPF2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowController mwc;
        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        private void SetupController()
        {
            mwc = new MainWindowController(this);
            RoutedEventHandler routed = new RoutedEventHandler(mwc.MainWindowMenuEventHandler);
            this.WrapLayoutMenuItem.Click += routed;
            this.GridLayoutDemoMenuItem.Click += routed;
        }
    }
}
